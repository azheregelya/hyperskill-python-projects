from typing import Optional, Callable, Iterable
from typing import NamedTuple
from functools import wraps

MenuEntry = NamedTuple('MenuEntry', [('caption', str), ('handler', Optional[Callable])])


def eoferror_to_none(func):
    """Decorator to catch EOFError and return None"""

    @wraps(func)
    def wrapped(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except EOFError:
            return None
    return wrapped


class ConsoleMenu:
    """Class to represent simple console menu required by the task"""

    def __init__(self, user_menu: Optional[Iterable[MenuEntry]] = None):
        self.menu: list[MenuEntry] = []
        if user_menu:
            self.menu.extend(user_menu)

    def add(self, caption: str, handler: Optional[Callable]) -> None:
        """Main interface to add menu entries"""
        self.menu.append(MenuEntry(caption, handler))

    def menu_loop(self) -> None:
        """Presumably generic implementation of menu loop
        """

        while True:
            choice = ''
            for num, item in enumerate(self.menu, 1):
                print(f'{num}. {item.caption}')
            try:
                # Skip empty answers
                while True:
                    choice = input("_> ")
                    if choice:
                        break

                func = self.menu[int(choice)-1].handler
            except (ValueError, IndexError):
                print(f'{choice} is not an option')
                continue
            except EOFError:
                return

            # Handle 'None' to return to previous menu
            if not func:
                break
            func()
