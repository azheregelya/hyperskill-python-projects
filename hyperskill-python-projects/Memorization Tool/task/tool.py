# write your code here
import sys
from console_menu import MenuEntry, ConsoleMenu, eoferror_to_none
from functools import partial
from typing import Optional
from flashcard import FlashCard


Store = list[FlashCard]


def add_flashcards(store: Store):
    """Add flashcard to cards store"""

    @eoferror_to_none
    def user_input_flashcard() -> FlashCard:
        """Reads flashcard from user input"""
        while True:
            question = input('Question: ').strip()
            if question:
                break
        while True:
            answer = input('Answer: ').strip()
            if answer:
                break
        return FlashCard(question, answer)

    def store_flashcard(store_: Store, card: Optional[FlashCard] = None):
        """Stores flashcard """
        if not card:
            card = user_input_flashcard()
        if card:
            store_.append(card)

    add_menu = ConsoleMenu()
    add_menu.add('Add a new flashcard', partial(store_flashcard, store_=store)),
    add_menu.add('Exit', None),
    add_menu.menu_loop()


def practice_flashcards(store: Store) -> None:
    """Go through all cards in the store"""

    @eoferror_to_none
    def train_flashcard(card: FlashCard) -> None:
        """Train one flashcard"""
        print('Question: ', card.question)
        print('Please press "y" to see the answer or press "n" to skip:')
        choice = input('_> ')
        if choice == 'y':
            print('Answer: ', card.answer)

    if not store:
        print('There is no flashcard to practice!')
        return

    for item in store:
        train_flashcard(item)


def loud_exit():
    """Because we can't just quit (-_-) """
    print('\nBye!')
    sys.exit()


def main():
    """Interaction entry point.

    Prepares Store, Main menu, enters to menu loop"""
    card_store = Store()
    main_menu = ConsoleMenu((
        MenuEntry('Add flashcards', partial(add_flashcards, store=card_store)),
        MenuEntry('Practice flashcards',  partial(practice_flashcards, store=card_store)),
        MenuEntry('Exit', loud_exit),))
    main_menu.menu_loop()


if __name__ == '__main__':
    main()
