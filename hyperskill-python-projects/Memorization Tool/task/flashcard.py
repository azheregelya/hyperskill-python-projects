class FlashCard:
    """Class to represent a single flashcard

    Later it has to be changed to ORM declared mapping.
    """
    def __init__(self, question: str, answer: str):
        self.question = question
        self.answer = answer
