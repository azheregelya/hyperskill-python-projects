# Kubernetes Crash Course for Absolute Beginners materials

Source: https://www.youtube.com/watch?v=s_o8dwzRlu4
Author: https://www.youtube.com/@TechWorldwithNana

Topics learned:
- K8S architecture
- How to create one-node cluster using Minukube
- How to write configuration files for ConfigMap, Secret, Deployment and Service components
- How to apply written configuration
- Got some troubleshooting experience due to my mistakes in config files.
